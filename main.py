from sample import load_samples
from advertisement import ApartmentSell,ApartmentRent,FlatSell,FlatRent,StoreRent,StoreSell

class Handler:
    ADVERTISEMENT_TYPES ={
        1:ApartmentSell,
        2:ApartmentRent,
        3:FlatSell,
        4:FlatRent,
        # 5:StoreSell,
        # 6:StoreRent
    }

    SWITCHES = {
        'r': 'get_report',
        's': 'show_all'
    }

    def get_report(self):
        for adv in self.ADVERTISEMENT_TYPES.values():
            print(adv, adv.manager.count())

    def show_all(self):
        for adv in self.ADVERTISEMENT_TYPES.values():
            for obj in adv.objects_list:
                obj.show_detail()


    def run(self):
        print('*'*10 ,'welecome','*'*10)
        for (key,value) in self.SWITCHES.items():
            print(f"press {key} for {value}")
        user_input= input("please enter your choice:")
        switch = self.SWITCHES.get(user_input,None)
        if switch is None:
            print('invalid input')
            self.run()
        choice = getattr(self,switch, None)
        choice()


if __name__ == '__main__':
    load_samples()
    handler = Handler()
    handler.run()