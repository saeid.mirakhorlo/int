from base import BaseClass


class User(BaseClass):
    def __init__(self,first_name,last_name,phone_number, *args, **kwargs) -> None:
        self.first_name = first_name
        self.last_name = last_name
        self.phone_number = phone_number
        
        super().__init__(*args, **kwargs)

    @property
    def fullname(self):
        return f"firstName: {self.first_name}, LaseName: {self.last_name}"
    