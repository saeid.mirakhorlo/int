from abc import ABC, abstractmethod

class EstateAbstract(ABC):
    def __init__(self, user, area, rooms_number, built_year, region, address, *args, **kwargs) -> None:
        self.user = user
        self.area = area
        self.rooms_number = rooms_number
        self.built_year = built_year
        self.region = region
        self.address = address
        super().__init__(*args, **kwargs)

    @abstractmethod
    def show_description(self):
        pass


class Apartment(EstateAbstract):
    def __init__(self, has_elevator, has_parking, floor, *args, **kwargs):
        self.has_elevator = has_elevator
        self.has_parking = has_parking
        self.floor = floor
        super().__init__(*args, **kwargs)


    def show_description(self):
        print(f"Apartment: {self.id}\t Area: {self.area}")


class Flat(EstateAbstract):
    def __init__(self, has_yard, stories, *args, **kwargs):
        self.has_yard = has_yard
        self.stories = stories
        super().__init__(*args, **kwargs)


    def show_description(self):
        print(f"Flat {self.id}\t Area: {self.area}")


class Store(EstateAbstract):



    def show_description(self):
        print(f"Store {self.id}\t Area: {self.area}")
