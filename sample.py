from random import choice
from estate import Apartment, Flat, Store
from region import Region
from user import User
from advertisement import ApartmentRent, ApartmentSell, FlatRent, FlatSell

FIRST_NAME = ['hamid', 'hamed', 'vahid', 'saeid']
LAST_NAME = ['Jamali', 'Kamali', 'Sarmadi', 'Aramesh']
PHONE_NUMBER = ['09123457892', '09213123123',
                '09353129873', '09192739128', '09353249872']


def load_samples():
    for number in PHONE_NUMBER:
        User(choice(FIRST_NAME), choice(LAST_NAME), number)

    # for user in User.objects_list:
    #     print(f"{user.id}, {user.fullname}")

    reg1 = Region(address="narmak")

    # apt1 = Apartment(has_elevator=False, has_parking=True,
    #                  user=User.objects_list[0], area=200, rooms_number=3, built_year=1394, region=reg1, address='west golbarg', floor=3)

    # apt1.show_description()

    # fl1 = Flat(has_yard=True, stories=3, user=User.object_list[2], area=420,
    #            rooms_number=4, built_year=1379, region=reg1, address='vanak square')
    # fl1.show_description()

    # st1 = Store(user=User.objects_list[-1], area=20, rooms_number=0,
    #             built_year=1385, region=reg1, address='Mirdamad')

    # st1.show_description()

    apartment_sell1 = ApartmentSell(
        has_elevator=False, has_parking=True,
        user=User.objects_list[0], area=200, rooms_number=3, built_year=1394, region=reg1, address='west golbarg', floor=3, price_per_meter=10, discount=True, swap=False
    )
    apartment_sell2 = ApartmentSell(
        has_elevator=True, has_parking=True,
        user=User.objects_list[0], area=60, rooms_number=1, built_year=1399, region=reg1, address='esat golbarg', floor=1, price_per_meter=8, discount=False, swap=False
    )


    apartment_rent = ApartmentRent(
        has_elevator=False, has_parking=True,
        user=User.objects_list[0], area=200, rooms_number=3, built_year=1394, region=reg1, address='west golbarg', floor=3, initial_price=10, monthly=3,convertable=False,discount=False
    )

    houes_sell = FlatSell(
        has_yard=True, stories=3, user=User.objects_list[2], area=420,
        rooms_number=4, built_year=1379, region=reg1, address='vanak square',
        price_per_meter=30, discount=False, swap=True

    )
    houes_sell.show_detail()

    house_rent = FlatRent(
        has_yard=True, stories=3, user=User.objects_list[2], area=420,
        rooms_number=4, built_year=1379, region=reg1, address='vanak square',
        initial_price=10, monthly=3,convertable=False,discount=False
    )

    # search_result = ApartmentSell.manager.search(region= reg1)
    # search_result = ApartmentSell.manager.search(price_per_meter__min = 1)
    search_result = ApartmentSell.manager.search(area__max= 300, area__min=10)
    # get_result = ApartmentSell.manager.get(region = reg1)
    print(search_result)
    # print(get_result)

    # house_rent_result = FlatRent.manager.search(region=reg1)
    # house_rent_result = FlatRent.manager.search(area = )
