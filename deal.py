from abc import ABC

class Sell(ABC):
    def __init__(self,price_per_meter,discount, swap, *args, **kwargs) -> None:
        self.price_per_meter = price_per_meter
        self.discount = discount
        self.swap = swap
        super().__init__(*args, **kwargs)

    def show_price(self):
        print(f'price: {self.price_per_meter}\t discount: {self.discount}\t swap: {self.swap}')

class Rent(ABC):
    def __init__(self,initial_price,monthly,convertable,discount, *args, **kwargs) -> None:
        self.initial_price = initial_price
        self.monthly = monthly
        self.convertable = convertable
        self.discount = discount
        super().__init__(*args, **kwargs)
    def show_price(self):
        print(f'price: {self.initial_price}\t monthly: {self.monthly}\t swap: {self.convertable}')
