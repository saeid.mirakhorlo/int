from base import BaseClass
from estate import Apartment,Flat,Store
from deal import Rent, Sell

class ApartmentSell(BaseClass,Apartment,Sell):
    def show_detail(self):
        self.show_description()
        self.show_price()

    def __str__(self) -> str:
        return f'Apartement id {self.id}, area : {self.area}'

class ApartmentRent(BaseClass,Apartment,Rent):
       def show_detail(self):
        self.show_description()
        self.show_price()

class FlatSell(BaseClass,Flat,Sell):
    def show_detail(self):
        self.show_description()
        self.show_price()

class FlatRent(BaseClass,Flat,Rent):
       def show_detail(self):
        self.show_description()
        self.show_price()

class StoreSell(BaseClass,Store,Sell):
       def show_detail(self):
        self.show_description()
        self.show_price()

class StoreRent(BaseClass,Store,Rent):
       def show_detail(self):
        self.show_description()
        self.show_price()
