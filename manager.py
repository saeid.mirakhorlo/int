class Manager:

    def __init__(self, _class=None) -> None:
        self._class = _class

    def search(self, **kwargs):
        results = []
        if len(kwargs.items()) > 1:
            min = None
            max = None
            mkey= None
            for key , value in kwargs.items():
                if key.endswith('__min'):
                    if mkey == None:
                        mkey = key[:-5]
                    min = value
                elif key.endswith('__max'):
                    if mkey == None:
                        mkey = key[:-5]
                    max = value
            for obj in self._class.objects_list:
                if hasattr(obj, mkey) and ((getattr(obj, mkey) >= min) and (getattr(obj,mkey) <=max)):
                    results.append(obj)
            return results
         
        else:
            for key, value in kwargs.items():
                if key.endswith('__min'):
                    key = key[:-5]
                    compare_key = 'min'
                elif key.endswith('__max'):
                    key = key[:-5]
                    compare_key = 'max'
                for obj in self._class.objects_list:
                    if hasattr(obj, key):
                        if compare_key == 'min':
                            result = getattr(obj, key) >= value
                        elif compare_key =='max':
                            result = getattr(obj, key) <= value
                        else:
                            result = getattr(obj, key) == value
                        if result:
                            results.append(obj)

                return results

    def get(self, **kwargs):
        for key, value in kwargs.items():
            for obj in self._class.objects_list:
                if hasattr(obj, key) and getattr(obj, key) == value:
                    return obj

    def count(self):
        return  len(self._class.objects_list)
            
